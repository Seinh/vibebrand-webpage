var util = require('util'),
    http = require('http'),
    mongo = require('mongodb'),
    express = require('express');

var app = express();

var webroot = '/Frontend',
    port = process.env.PORT || 1337;

var mongoUri =  process.env.MONGOLAB_URI ||
                process.env.MONGOHQ_URL ||
                'mongodb://localhost/test';

mongoUri += "?safe=true&strict=false";

function insert(col, value, callback) {
    mongo.Db.connect(mongoUri, function (err, db) {
      db.collection(col, function(er, collection) {
        collection.insert(value, function(err, result){
            callback(err, result);
        });
      });
    });
}

function get(col, callback) {
    mongo.Db.connect(mongoUri, function (err, db) {
      db.collection(col, function(er, collection) {
        collection.find().toArray(function(err, items){
            callback(err, items);
        });
      });
    });
}

function start() {

    app.use(express.static(__dirname + webroot));

    app.get("/insert/:col/:value", function(req, res, next){
        res.send(util.format("Insertando %s en %s", req.params.col, req.params.value));
        callback = function(error, result){
            //use error and result
        };
        insert(req.params.col, {"value": req.params.value}, callback);
    });

    app.get("/get/:col", function(req, res, next){
        callback = function(error, items){
            res.send({"result": items});
        };
        get(req.params.col, callback);
    });
    app.listen(port, '0.0.0.0');
    console.log('Server running');

}

exports.start = start;
